menu_input = 22
garbanzos = [0, 0]
lentejas = [0, 0]
arvejas = [0, 0]
# en la posicion 0 se almacena la cantidad, en la 2 se almacena el precio
print("\t\t Granos.Inc \nPresione el numero para realizar su operacion\n")


def ingresar_granos():  # opcion 1
    print("\nCantidad de garbanzos: ")
    garbanzos[0] = int(input())

    print("\nCantidad de lentejas: ")
    lentejas[0] = int(input())

    print("\nCantidad de arvejas: ")
    arvejas[0] = int(input())

    print("En inventario se tienen: " + str(garbanzos[0]) + " garbanzos, " + str(arvejas[0]) + " arvejas y " +
    str(lentejas[0]) + " lentejas")

    print("Para volver al menu presione 0")  # volver al menu principal
    menu_input = input()
    if menu_input == '0':
        menu()


def costo_granos():
    print("Costo de garbanzos en $: ")
    garbanzos[1] = int(input())

    print("Costo de lentejas en $: ")
    lentejas[1] = int(input())

    print("Costo de arvejas en $: ")
    arvejas[1] = int(input())

    print("Sus garbanzos cuestan: $")
    print(garbanzos[1] * garbanzos[0])

    print("Sus lentejas cuestan: $")
    print(lentejas[1] * lentejas[0])

    print("Sus arvejas cuestan: $")
    print(arvejas[1] * arvejas[0])

    print("Para volver al menu presione 0")
    menu_input = input()
    if menu_input == '0':
        menu()


def costo_total():
    x = (arvejas[1] * arvejas[0]) + (lentejas[1] * lentejas[0]) + (garbanzos[1] * garbanzos[0])
    print("El costo total de todos los granos es $: " + str(x))

    print("Para volver al menu presione 0")
    menu_input = input()
    if menu_input == '0':
        menu()


def menu():  # menu principal del programa
    print("1.Ingresar grano\n2.Calcular costo por tipo de grano\n3.Calcular costo total\n4.Terminar")
    menu_input = input()
    if menu_input == '1':
        ingresar_granos()
    elif menu_input == '2':
        costo_granos()
    elif menu_input == '3':
        costo_total()
    else:
        return 0


menu()